from flask import Flask
from flask import jsonify
from flask import request
from flask import abort


app = Flask(__name__)
people = [
        {"id":"p1", "name":"Gayatri", "age":20, "addr":"sangli"},
        {"id":"p2", "name":"Priya", "age":20, "addr":"bengal"},
        {"id":"p3", "name":"Vaishnavi", "age":20, "addr":"pune"},
        {"id":"p4", "name":"Siddhika", "age":20, "addr":"pune"}
    ]


@app.route("/hello")
def hello_world():
    return "Hello, World!"


@app.route("/persons")
def get_all_person():
    return jsonify(people)


@app.route("/persons/<id>", methods=["GET"])
def get_person(id):
    for p in people:
        if p["id"] == id:
            return p
    return jsonify(None)


@app.route("/persons", methods=["POST"])
def add_person():
    if not request.json:
        abort(400)
    people.append(request.json)
    return {"status": "success"}


if __name__ == "__main__":
    app.run()

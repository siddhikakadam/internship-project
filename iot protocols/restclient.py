#!/usr/bin/python3

from requests import get
from requests import post

base_url = "http://localhost:5000/persons"

person = get(base_url + "/p1")
if person.status_code == 200:
    print(person.json())
else:
    print(person)

person = {"id": "p6", "name": "Nitin", "age":42, "addr": "Pachgani"}
result = post(base_url, json=person)
if result.status_code == 200:
    print(result.json())
else:
    print(result)

people = get(base_url)
if people.status_code == 200:
    print(people.json())
else:
    print(people)




#!/usr/bin/python3

import paho.mqtt.client as mqtt
from time import sleep


client = mqtt.Client()
client.connect("127.0.0.1", 1883, 60)

for i in range(101,110):
    client.publish("internship", str(i))
    print("sent: " + str(i))
    sleep(1)

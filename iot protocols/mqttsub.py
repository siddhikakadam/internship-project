#!/usr/bin/python3

import paho.mqtt.client as mqtt


def on_connect(client, userdata, flags, rc):
    print("MQTT Client Connected.")


def on_message(client, userdata, msg):
    data = msg.payload.decode("utf-8")
    print("MQTT Data Received : " + msg.topic + " --> " + data)


client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect("127.0.0.1", 1883, 60)
client.subscribe("internship")

print("subscriber waiting for messages ...")
client.loop_forever()

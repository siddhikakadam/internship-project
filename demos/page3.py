# int add(int n1, int n2)
# {
#    int answer = n1 + n2;
#    printf("anwer = %f", answer);
#    return answer;
# }
# add (10, 20);
# add ('10', '20');

# function
# def <function name>(<params>):
#   function body


# parameterless function
# function declaration
def function1():
    print("inside function1 - 1")
    print("inside function1 - 2")

# print("outside function1")


# function1()

# parameterized function
def function2(p1):
    print("inside function2")
    print("p1 =", p1)
    print("type of p1 =", type(p1))


# int
# function2(10)

# float
# function2(5.6)

# str
# function2("i20")

# bool
# function2(True)


def add(p1, p2):
    addition = p1 + p2
    print("addition =", addition)


# add(20, 30)
# add("20", "30")


def multiply(p1, p2):
    multiplication = p1 * p2
    return multiplication


answer = multiply(3, 4)
print("answer =", answer)












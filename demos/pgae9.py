# import page1 with myPage as its alias
import page8 as myPage

# will be called from page1
myPage.add(10, 20)


def add(p1, p2):
    print("inside page4")
    print("{p1}+{p2}={p1+p2}")

# will be called from page4
add(40, 50)


# import add from page1 with alias as myAdd
from page8 import add as myAdd

# will be called from page1
myAdd(40, 50)

#import product.product
#import faculty.faculty
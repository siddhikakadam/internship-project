# collection
# - collection of similar or dissimilar values
# - types
#   - list        => [] => mutable (modifiable: append, insert, pop, sort etc)
#     - allows duplicate values
#   - tuple       => () => immutable (does not allow to modify the collection dynamically)
#     - allows duplicate values
#   - dictionary  => {} => mutable
#     - allows duplicate values
#     - does not allow duplicate keys
#   - set         => {}
#     - collection of unique values
#     - does not allow duplicate values


def function1():
    # empty list
    # numbers = []
    numbers = list()
    print("type of numbers =", type(numbers))


function1()


def function2():
    # list of ints
    numbers = [10, 20, 30, 40, 50]
    # print(numbers)
    # for..in loop
    # for temp in numbers:
    #     print("number =", temp)

    # indexing
    print("value at 2nd index =", numbers[2])

    # -1 points to the last value in the collection
    print("value at -1 index =", numbers[-1])
    print("value at 4 index =", numbers[4])

    print("value at -2 index =", numbers[-2])

    # slicing
    # - upper bound is always excluded
    print("slicing: ", numbers[0:3])


    # index = 0
    # for number in numbers:
    #     if number == 40:
    #         print("40 is present on index =", index)
    #         break
    #     index += 1
    print("40 is present on index =", numbers.index(40))


# function2()


def function3():
    # list of strings
    countries = ["india", "usa", "uk", "russia"]
    print(countries)

    # for country in countries:
    #     print("country:", country)

    # append a new value
    countries.append("germany")
    print(countries)

    # remove the last value
    country = countries.pop()
    print("popped value =", country)
    print(countries)

    # remove a value by index
    country = countries.pop(1)
    print("popped value =", country)
    print(countries)

    # insert a new value in-between
    countries.insert(2, "france")
    print(countries)

    # count of the values
    print("number of values in countries =", len(countries))

    # replace a value
    countries[2] = "Japan"
    print(countries)


# function3()


def function4():
    persons = []

    # start: 0, stop: 10 (excluded), step: 1
    index_values = list(range(0, 3, 1))
    for index in index_values:
        # to get an input from user
        p1 = input("enter person name: ")
        persons.append(p1)

    print(persons)


function4()


def function5():
    # tuple
    t1 = (10, 20, 30, 40, 50)

    print("numbers in t1 =", len(t1))
    print(t1)

    # t1[0] = 100
    # t1.append(60)
    # t1.pop()

    # delete the memory
    del t1


# function5()


def function6():
    person1 = ["person1", "person1@test.com", 30, True]
    # print(person1)
    # for info in person1:
    #     print("info =", info)
    #     print("type of info =", type(info))

    print("name =", person1[0])
    print("email =", person1[1])
    print("age =", person1[2])
    print("eligible for voting =", person1[3])

    person2 = ("person2", 30, "person2@test.com", True)

    print("name =", person2[0])
    print("email =", person2[1])
    print("age =", person2[2])
    print("eligible for voting =", person2[3])


# function6()


def function7():
    # dictionary:
    # - collection of key-value pairs
    # - where
    #   - every key has to be a string
    #   - a value can be of any data type
    person1 = {
        "name": "person1",
        "email": "person1@test.com",
        "age": 10,
        "canVote": False
    }

    # print("person1 =", person1)
    # print("type of person1 =", type(person1))

    print("name =", person1["name"])
    print("email =", person1["email"])
    print("age =", person1["age"])
    print("canVote =", person1["canVote"])

    if person1["canVote"] == True:
        print("Yes. The person is eligible for voting. :)")
    else:
        print("No. The person is NOT eligible for voting. :(")

    person2 = {
        "age": 60,
        "canVote": True,
        "email": "person2@test.com",
        "name": "person2"
    }

    print("name =", person2.get("name"))
    print("email =", person2.get("email"))
    print("age =", person2.get("age"))
    print("canVote =", person2.get("canVote"))

    if person2["canVote"] == True:
        print("Yes. The person is eligible for voting. :)")
    else:
        print("No. The person is NOT eligible for voting. :(")

    person2["name"] = "person3"
    print("name =", person2["name"])

    print("keys =", person1.keys())
    print("values =", person1.values())


# function7()


def function8():
    # list
    numbers_1 = [10, 20, 30, 40, 50, 10, 20, 30, 40, 50, 10, 20, 30, 40, 50]

    # tuple
    numbers_2 = (10, 20, 30, 40, 50, 10, 20, 30, 40, 50, 10, 20, 30, 40, 50)

    # set
    numbers_3 = {10, 20, 30, 40, 50, 10, 20, 30, 40, 50, 10, 20, 30, 40, 50}

    print(numbers_1)
    print("type of numbers_1 =", type(numbers_1))

    print(numbers_2)
    print("type of numbers_2 =", type(numbers_2))

    print(numbers_3)
    print("type of numbers_3 =", type(numbers_3))


# function8()


def function9():
    # list of dictionaries
    cars = [
        {"model": "i20", "company": "hyundai", "price": 7.5}, # 0
        {"model": "i10", "company": "hyundai", "price": 5.5}, # 1
        {"model": "nano", "company": "tata", "price": 1.5}    # 2
    ]

    # print(cars)
    # print("model =", cars[0])
    # print("model =", cars[1])
    # print("model =", cars[2])

    for car in cars:
        # print(car)
        print("model =", car["model"])
        print("company =", car.get("company"))
        print("price =", car["price"])
        print()

    print("company =", cars[2]["company"])
    print("company =", cars[2].get("company"))


function9()


def function10():
    # list of tuples
    # collection of collections => multi-dimensional collection
    mobiles = [
         # 0              1        2
        ("iphone xs max", "apple", 144000), # 0
        ("z10", "blackberry", 40000),       # 1
        ("galaxy s10", "samsung", 78000)    # 2
    ]

    # for mobile in mobiles:
    #     # print(mobile)
    #     print("model =", mobile[0])
    #     print("company =", mobile[1])
    #     print("price =", mobile[2])
    #     print()

    print("model = ", mobiles[0][0])


# function10()


# module
# - a file with .py extension
# - collection of variables, functions etc


def add(p1, p2):
    addition = p1 + p2
   # print(f"{p1} + {p2} = {addition}")
    # print("{} + {} = {}".format(p1, p2, addition))
    print("addition: ", addition)




def multiply(p1, p2):
    multiplication = p1 * p2
    #print(f"{p1} * {p2} = {multiplication}")
    print("multiplication: ", multiplication)


# print(f"in page1: {__name__}")

# execute this code only when
# page1 starts execution
if __name__ == '__main__':
    add(10, 20)
    multiply(3, 6)


def function1(p1):
    print("inside function1")
    print("p1 =", p1)


# function1(10)
# function1(p1=10)

def function2(p1, p2):
    print("inside function2")
    print("p1 =", p1)
    print("p2 =", p2)


# function2(10, 20)
# function2(p1=10, p2=20)
# function2(p2=20, p1=10)


# default value
# optional parameter(s)
def function3(p1=0, p2=0):
    print("inside function3")
    print("p1 =", p1)
    print("p2 =", p2)


# p2 = 20
function3(10, 20)

# p2 = 0
function3(10)

# p1 = 0, p2 = 0
function3()

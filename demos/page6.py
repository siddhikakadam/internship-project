def function1():
    # open the file
    file = open('./my_file.txt', 'r')

    # perform the operation
    # data = file.read()
    # print(data)

    data = file.readlines()
    # print(data)

    for line in data:
        print(line)

    # close the file
    file.close()


function1()


def function2():
    file = open('./my_file.txt', 'w')
    file.write("India is my country.\n")
    file.write("All indians are my brothers and sisters.\n")
    file.write("I love my country.\n")
    file.close()


# function2()

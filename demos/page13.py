import math

print("pi = {math.pi}")
print("e = {math.e}")
print("2^14 = {math.pow(2, 14)}")
print("2^32 = {math.pow(2, 32)}")
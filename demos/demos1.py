print("hello world")
# int
num=100
print("num",num)
print("type of num",type(num))
# float
salary = 5.6
print("salary = ", salary)
print("type of salary =", type(salary))
# str
name = "steve"
print("name = ", name)
print("type of name =", type(name))
# str
company = 'Apple'
print("company = ", company)
print("type of company =", type(company))

# str
ch = 'a'
print("ch = ", ch)
print("type of ch =", type(ch))

# bool
canVote = False
print("canVote = ", canVote)
print("type of canVote =", type(canVote))



